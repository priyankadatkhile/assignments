﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Assignment1.Models
{
    public class Role1
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        
        public DataSet StoreAllRoles { get; set; }
    }
}