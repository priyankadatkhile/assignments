﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;
using Assignment1.Models;

namespace Assignment1.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            string str = @"Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";

            using (SqlConnection con = new SqlConnection(str))
            {
                //SqlCommand cmd = new SqlCommand("Insert into Users UserId, Name, username, Pass, Email, number, createdDate, RoleId values (@UserId, @Name, @username, @Pass, @Email, @number, @createdDate, @RoleId)", con);

                SqlCommand cmd = new SqlCommand("Admin", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@username", user.username);
                cmd.Parameters.AddWithValue("@Pass", user.Pass);
                //cmd.Parameters.AddWithValue("@RoleId", user.RoleId);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            
            }

            return RedirectToAction("Admin", "Admin");
            //return View();

            
        }

        public ActionResult Admin()
        {
           return View();
        }

    }
}
