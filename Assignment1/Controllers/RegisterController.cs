﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment1.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Assignment1.Controllers
{
    public class RegisterController : Controller
    {
        //
        // GET: /Register/

        assignment1Entities userdb = new assignment1Entities();
        public ActionResult Register()
        {
            assignment1Entities myentity = new assignment1Entities();
            var getRoleName = myentity.Roles.ToList();
            SelectList list = new SelectList(getRoleName, "RoleId", "RoleName");
            ViewBag.RoleListName = list;
            return View();
        }

        public ActionResult Insertdate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Insertdate(User user)
        {
          //  userdb.InsertUser(user.UserId, user.Name, user.username, user.Pass, user.Email, user.number, user.createdDate, user.RoleId);

            assignment1Entities myentity = new assignment1Entities();
            var getRoleName = myentity.Roles.ToList();
            SelectList list = new SelectList(getRoleName, "RoleId", "RoleName");
            ViewBag.RoleListName = list;

            string str = @"Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
            DateTime date = Convert.ToDateTime(DateTime.Now.ToShortDateString());

            using (SqlConnection con = new SqlConnection(str))
            {
                //SqlCommand cmd = new SqlCommand("Insert into Users UserId, Name, username, Pass, Email, number, createdDate, RoleId values (@UserId, @Name, @username, @Pass, @Email, @number, @createdDate, @RoleId)", con);

                SqlCommand cmd = new SqlCommand("InsertUsers", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", user.UserId);
                cmd.Parameters.AddWithValue("@Name", user.Name);
                cmd.Parameters.AddWithValue("@username", user.username);
                cmd.Parameters.AddWithValue("@Pass", user.Pass);
                cmd.Parameters.AddWithValue("@Email", user.Email);
                cmd.Parameters.AddWithValue("@number", user.number);
                cmd.Parameters.AddWithValue("@createdDate", date);
                cmd.Parameters.AddWithValue("@RoleId", user.RoleId);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            return View();
        }

        //public ActionResult Insertdate(User uesr)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        ds.Users(user)
        //    }
        //    return View();
        //}

    }
}
