﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment1.Models;
using System.Data;

namespace Assignment1.Controllers
{
    public class RolesController : Controller
    {
        //
        // GET: /Roles/
        Assignment1.Database.Roles obj1 = new Database.Roles();

        public ActionResult Roles()
        {
            return View();
        }

        public ActionResult RolesGetAll()
        {
          Role1 location = new Role1();
            location.StoreAllRoles = obj1.SelectAllDataRoles();


            List<Role> searchList = new List<Role>();
            foreach (DataRow dr in location.StoreAllRoles.Tables[0].Rows)
            {
                searchList.Add(new Role
                {
                    RoleId = Convert.ToInt32(dr["RoleId"]),
                    RoleName = dr["RoleName"].ToString()
                });

            }
            return new JsonResult { Data = searchList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult DeleteRoles(int RoleId)
        {
            string _Del = null;
            try
            {
                string RoleName = obj1.Delete(Convert.ToInt32(RoleId));

                _Del = "Role Deleted Successfully";


            }
            catch (Exception)
            {
                throw;
            }
            return new JsonResult { Data = _Del, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}

