﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using Assignment1.Models;
using System.Configuration;

namespace Assignment1.Database
{
    public class Roles
    {
        int ID = 0;
        string RolesName = "";
        private SqlConnection con;
        private void Connect()
        {
            string Constring = ConfigurationManager.ConnectionStrings["assignment1Entities"].ToString();
            con = new SqlConnection(Constring);
        }

        //public bool Save(assignment1Entities obj)
        //{
        //    Connect();
        //    SqlCommand cmd = new SqlCommand("IURole", con);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    if (obj.Roles == null || obj.Roles == "")
        //    {
        //        cmd.Parameters.AddWithValue("@RoleName", 0);
        //    }
        //    else
        //    {
        //        cmd.Parameters.AddWithValue("@RoleName", obj.Roles);
        //    }

        //    if (obj.Id == 0)
        //    {
        //        cmd.Parameters.AddWithValue("@RoleID", 0);
        //        cmd.Parameters.AddWithValue("@Mode", "Add");
        //    }
        //    else
        //    {
        //        cmd.Parameters.AddWithValue("@RoleID", obj.Id);
        //        cmd.Parameters.AddWithValue("@Mode", "Edit");
        //    }
        //    //  cmd.Parameters.AddWithValue("@Mode", "Add");
        //    con.Open();
        //    int i = cmd.ExecuteNonQuery();
        //    con.Close();
        //    if (i > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        public DataSet SelectAllDataRoles()
        {
            Connect();
            DataSet ds = null;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllRoles", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RolesID", ID);
                cmd.Parameters.AddWithValue("@RolesName", RolesName);
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);
                con.Close();
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
        }

        public string Delete(int RoleId)
        {
            string Table = string.Empty;
            try
            {
                Connect();
                SqlCommand cmd = new SqlCommand("DeleteRoles", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RoleId", RoleId);

                con.Open();
                int i = cmd.ExecuteNonQuery();

                con.Close();
                if (i > 0)
                {
                    return Table;
                }
                else
                {
                    return Table;
                }
            }
            catch (Exception)
            {
                return Table;
            }
        }
    }
}